/* eslint func-names: [0] */
import _ from 'lodash';
import Promise from 'bluebird';
import winston from 'winston';
import debug from 'debug';
import lunr from 'lunr';
import {utils} from 'littlefork';

const d = debug('❤ plugin.search');

// Setup logging.
winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {timestamp: true, colorize: true});

const indexer = (id, fields) =>
  // Use `function` because of the API using `this`. Exception added in eslint.
  lunr(function () {
    _.forEach(fields, f => this.field(f));
    this.ref(id);
  });

const search = val => {
  utils.assertEnv(['SEARCH_QUERIES']);
  d(`Searching for ${process.env.SEARCH_QUERIES}`);
  const queries = _.split(process.env.SEARCH_QUERIES, ',');
  const idField = '_lf_id_hash';
  const titleField = _.first(val.data)._lf_title;
  const contentField = _.first(val.data)._lf_content;
  const unitFields = [idField, titleField, contentField];

  const index = _.reduce(val.data, (memo, unit) => {
    const doc = _.pick(unit, unitFields);
    memo.add(doc);
    return memo;
  }, indexer(idField, unitFields));

  const envelope = _.reduce(queries, (memo, query) => {
    const matches = index.search(query);

    winston.info(`Got ${_.size(matches)} hits searching for ${query}.`);

    return _.merge(memo, {data: _.map(memo.data, unit => {
      const searchResult = _.find(matches, {ref: unit._lf_id_hash});
      return _.merge(unit, searchResult ?
                     {_lf_search: {[query]: _.pick(searchResult, ['score'])}} :
                     undefined);
    })});
  }, val);

  return Promise.resolve(envelope);
};

search.argv = {
  'search.queries': {
    type: 'string',
    nargs: 1,
    desc: 'The queries used for search.',
  },
};

export {search};
