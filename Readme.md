# littlefork-plugin-search

This is a plugin for [littlefork](https://github.com/tacticaltech/littlefork).

It searches the title and content of data units. This plugin is based on
[lunr](http://lunrjs.com/).

## Installation

```
npm install --save littlefork-plugin-search
```

## Usage

This plugin exports a single transformation plugin:

### `search` transformation

```
$(npm bin)/littlefork -c cfg.json -p ddg,search --search.query "arnold schwarzenegger"
```

This searches the contents of the fields specified in `_lf_title` and
`_lf_content`. It extends each data unit with the `_lf_search` field. If there
is a match, the fields gets populated with a score (`score`)

## Configuration

- `search.queries`

The search queries to search for. This uses the `lunr.js` query
format. Separate multiple queries with a comma, e.g.:

    --search.queries cookie,telecommunicatie
